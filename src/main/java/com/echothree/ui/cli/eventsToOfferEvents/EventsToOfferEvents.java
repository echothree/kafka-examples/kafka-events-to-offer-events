// --------------------------------------------------------------------------------
// Copyright 2002-2022 Echo Three, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// --------------------------------------------------------------------------------

package com.echothree.ui.cli.eventsToOfferEvents;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventsToOfferEvents
        extends BaseGraphQlEventRelay {

    private static final Logger LOG = LoggerFactory.getLogger(EventsToOfferEvents.class);

    public EventsToOfferEvents(final String bootstrapServers, final String incomingTopic, final String outgoingTopic,
            final String username, final String password) {
        super(bootstrapServers, incomingTopic, outgoingTopic, username, password);
    }

    protected String executeOfferQuery(final CloseableHttpClient client, final String id)
            throws Exception {
        var responseJson = executeUsingPost("""
            query {
                offer(
                    id: "%s"
                ) {
                    offerName
                    description
                    id
                }
            }
            """.formatted(id), client);

        // Remove the 'data' wrapping the item.
        return toJson(getMap(toMap(responseJson), "data"));
    }

    @Override
    protected String execute(final String id, final CloseableHttpClient client)
            throws Exception {
        var hasErrors = executeEmployeeLoginMutation(client);
        String json = null;

        if(!hasErrors){
            json = executeOfferQuery(client, id);
        }

        return json;
    }

    public void run()
            throws Exception {
        var groupId = EventsToOfferEvents.class.getSimpleName();

        eventLoop(groupId, "ECHOTHREE", "Offer");
    }

}
