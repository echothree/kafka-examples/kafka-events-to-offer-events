// --------------------------------------------------------------------------------
// Copyright 2002-2022 Echo Three, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// --------------------------------------------------------------------------------

package com.echothree.ui.cli.eventsToOfferEvents;

import com.echothree.ui.cli.eventsToOfferEvents.schema.Event;
import com.echothree.ui.cli.eventsToOfferEvents.schema.EventAdapter;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.common.net.MediaType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.ToNumberPolicy;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.Thread.sleep;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseEventRelay {

    private static final Logger LOG = LoggerFactory.getLogger(BaseEventRelay.class);

    protected static final Pattern ENTITY_REF_PATTERN = Pattern.compile("^([a-zA-Z0-9-_]+)\\.([a-zA-Z0-9-_]+)\\.([0-9]+)$");

    protected static final Gson GSON = new GsonBuilder()
            .serializeNulls()
            .setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE)
            .registerTypeAdapter(Event.class, new EventAdapter())
            .create();

    protected final Headers HEADERS_JSON = new RecordHeaders()
            .add(new RecordHeader(HttpHeaders.CONTENT_TYPE, MediaType.JSON_UTF_8.toString().getBytes(Charsets.UTF_8)));

    protected final String bootstrapServers;
    protected final String incomingTopic;
    protected final String outgoingTopic;
    protected final String username;
    protected final String password;

    public BaseEventRelay(final String bootstrapServers, final String incomingTopic, final String outgoingTopic,
            final String username, final String password) {
        this.bootstrapServers = bootstrapServers;
        this.incomingTopic = incomingTopic;
        this.outgoingTopic = outgoingTopic;
        this.username = username;
        this.password = password;
    }

    public Map<String, Object> toMap(String json) {
        var typeToken = new TypeToken<Map<String, Object>>() {
        };

        Map<String, Object> map = GSON.fromJson(json, typeToken.getType());

        return map == null ? Collections.emptyMap() : map;
    }

    public String toJson(Map<String, Object> map)  {
        return GSON.toJson(map);
    }

    public boolean getBoolean(Object bean, String name)
            throws Exception {
        return (Boolean)PropertyUtils.getProperty(bean, name);
    }

    public String getString(Object bean, String name)
            throws Exception {
        return (String)PropertyUtils.getProperty(bean, name);
    }

    protected Map<String, Object> getMap(Object bean, String name)
            throws Exception {
        return (Map<String, Object>)PropertyUtils.getProperty(bean, name);
    }

    public void eventLoop(final String groupId, final String componentVendorName, final String entityTypeName)
            throws Exception {
        // Consumer Configuration
        var consumerProperties = new Properties();
        consumerProperties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        consumerProperties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerProperties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerProperties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        consumerProperties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerProperties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, Boolean.FALSE.toString());

        // Producer Configuration
        var producerProperties = new Properties();
        producerProperties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        producerProperties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producerProperties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // Create Consumer
        try(var consumer = new KafkaConsumer<String, String>(consumerProperties)) {
            consumer.subscribe(List.of(incomingTopic));

            // Create Producer
            try(var producer = new KafkaProducer<String, String>(producerProperties)) {
                while(true) {
                    var records = consumer.poll(Duration.ofMillis(100));

                    for (var partition : records.partitions()) {
                        var partitionRecords = records.records(partition);
                        for (var record : partitionRecords) {
                            processRecord(record, componentVendorName, entityTypeName, producer);
                        }

                        // "at-least-once" delivery guarantee
                        var lastOffset = partitionRecords.get(partitionRecords.size() - 1).offset();
                        consumer.commitSync(Collections.singletonMap(partition, new OffsetAndMetadata(lastOffset + 1)));
                    }
                }
            }
        }
    }

    public void processRecord(ConsumerRecord<String, String> record, final String componentVendorName,
            final String entityTypeName, Producer<String, String> producer)
            throws Exception {
        var value = record.value();

//        LOG.info("Key: " + record.key() + ", Value: " + record.value());
//        LOG.info("Partition: " + record.partition() + ", Offset:" + record.offset());

        var event = GSON.fromJson(value, Event.class);
        var entityRef = event.entityRef();
        var eventTypeName = event.eventTypeName();
//        LOG.info("entityRef = " + entityRef + ", entityTypeName = " + eventTypeName);

        if(!"READ".equals(eventTypeName)) {
            var matcher = ENTITY_REF_PATTERN.matcher(entityRef);
            if(matcher.matches()) {
                var eventComponentVendorName = matcher.group(1);
                var eventEntityTypeName = matcher.group(2);

                if(eventComponentVendorName.equals(componentVendorName)) {
                    if(eventEntityTypeName.equals(entityTypeName)) {
                        if(!"DELETE".equals(eventTypeName)) {
                            sleep(500);
                        }

                        sendEventToTopic(producer, value, event);
                    }
                }
            }
        }
    }

    protected String executeUsingPost(String query, final CloseableHttpClient client)
            throws URISyntaxException, IOException {
        String responseJson = null;
        HttpPost httpPost = new HttpPost(new URIBuilder().setScheme("https")
                .setHost("127.0.0.1")
                .setPath("/graphql").build());
        httpPost.setHeader("Content-type", "application/json");

        Map<String, Object> map = new HashMap<>();

        if(query != null) {
            map.put("query", query);
        }

        var requestEntity = new StringEntity(GSON.toJson(map));
        httpPost.setEntity(requestEntity);

        try(var closeableHttpResponse = client.execute(httpPost)) {
            var statusCode = closeableHttpResponse.getStatusLine().getStatusCode();

            if(statusCode == 200) {
                var responseEntity = closeableHttpResponse.getEntity();

                if(responseEntity != null) {
                    responseJson = CharStreams.toString(new InputStreamReader(responseEntity.getContent(), Charsets.UTF_8));
                }

                EntityUtils.consume(responseEntity);
            } else {
                LOG.error("statusCode = " + statusCode);
            }
        }
        return responseJson;
    }

    protected abstract String idToEntityJson(final String id)
            throws Exception;

    protected void sendEventToTopic(final Producer<String, String> producer, final String eventJson,
            final Event event)
            throws Exception {
        var eventTypeName = event.eventTypeName();
        var isDelete = "DELETE".equals(eventTypeName) ;
        var entityJson = isDelete ? null : idToEntityJson(event.id());

        // There should only be one top-level entry in the entityJson. Use this name, just as "item" and merge
        // it into new JSON that has both the original "event" and the "item" at the top level.
        var entityMap = isDelete ? null : toMap(entityJson);
        var entityName = isDelete ? null : entityMap.keySet().stream().findFirst();

        if(isDelete || entityName.isPresent()) {
            // Merge eventJson and entityJson into the same chunk of JSON.
            var valueMap = new LinkedHashMap<String, Object>();
            valueMap.put("event", toMap(eventJson));

            if(!isDelete) {
                valueMap.put(entityName.get(), getMap(entityMap, entityName.get()));
            }

            // Turn it all back into JSON
            var valueJson = toJson(valueMap);
            LOG.info("valueJson = " + valueJson);

            send(producer, event, valueJson);
        } else {
            LOG.error("entityName not present");
        }
    }

    protected void send(final Producer<String, String> producer, final Event event, final String valueJson)
            throws InterruptedException, ExecutionException {
        var future = producer.send(new ProducerRecord<>(outgoingTopic, null,
                event.eventTime(), event.entityRef(), valueJson, HEADERS_JSON));

        future.get();
        producer.flush();
    }

}
