// --------------------------------------------------------------------------------
// Copyright 2002-2022 Echo Three, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// --------------------------------------------------------------------------------

package com.echothree.ui.cli.eventsToOfferEvents;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseGraphQlEventRelay
        extends BaseEventRelay {

    private static final Logger LOG = LoggerFactory.getLogger(BaseGraphQlEventRelay.class);

    public BaseGraphQlEventRelay(final String bootstrapServers, final String incomingTopic, final String outgoingTopic,
            final String username, final String password) {
        super(bootstrapServers, incomingTopic, outgoingTopic, username, password);
    }

    protected boolean executeEmployeeLoginMutation(final CloseableHttpClient client)
            throws Exception {
        var responseJson = executeUsingPost("""
            mutation {
                employeeLogin(input: {
                    username: "%s",
                    password: "%s",
                    companyName: "TEST_COMPANY", clientMutationId: "1"
                }) {
                    commandResult {
                        hasErrors
                    }
                }
            }
            """.formatted(username, password), client);

        var body = toMap(responseJson);
        var hasErrors = getBoolean(body, "data.employeeLogin.commandResult.hasErrors");

        if(hasErrors) {
            LOG.error(responseJson);
        }

        return hasErrors;
    }

    protected abstract String execute(final String id, final CloseableHttpClient client)
            throws Exception;

    @Override
    protected String idToEntityJson(final String id)
            throws Exception {
        String json;

        var sslSocketFactory = new SSLConnectionSocketFactory(
                SSLContexts.custom()
                        .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                        .build(),
                NoopHostnameVerifier.INSTANCE);

        try(var client = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setSocketTimeout(5000)
                        .setConnectTimeout(5000)
                        .setConnectionRequestTimeout(5000)
                        .build())
                .setSSLSocketFactory(sslSocketFactory)
                .build()) {

            json = execute(id, client);
        }

        return json;
    }

}
